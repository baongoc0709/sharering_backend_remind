FROM node:8-alpine
WORKDIR /var/workspace

# update the repository sources list
# and install dependencies
#RUN apt-get update \
#    && apt-get install -y curl git python build-essential \
#    && apt-get -y autoclean
RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh
RUN npm install -g pm2
RUN  echo "    IdentityFile ~/.ssh/id_rsa" >> /etc/ssh/ssh_config
RUN  echo "    StrictHostKeyChecking no " >> /etc/ssh/ssh_config


COPY package.json .
COPY package-lock.json .
RUN npm install && \
    npm cache clean --force


EXPOSE 7000
