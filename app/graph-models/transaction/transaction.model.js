const mongoose = require('mongoose')
const Schema = mongoose.Schema

const logTransactionSHRP = new Schema({
  transaction_id : { type: String, required: true  }, //transaction top up , backend create
  sender_address: { type: String, required: true  },
  receive_address: { type: String, required: true  },
  type     : { type: Number,required: true ,default : 0 }, //0 : top up , 1 withdraw , 2 transfer SHRP  from 2 user
  status   : { type: Number,required: true ,default : 0 }, //0 : pending , 1 completed , 2 withdraw - create orderWD
  payment_method   : { type: String, required: true  },
  is_paid : { type: Number,required: true ,default : 0 }, //0 :not yet , 1 : ongoing ,2 :finished
  blockchain_transaction_hash : { type: String }, // available when status is completed
  note     : { type: String },
  data     : { type :Object },
  _created: { type: Date, required: true, default: Date.now },
  _updated: { type: Date, required: true, default: Date.now }
})
logTransactionSHRP.pre('update', function() {
  this.update({},{ $set: { _updated: new Date() } })
})
const logTransshrp   = mongoose.model('logTransactionSHRP', logTransactionSHRP)
module.exports = { logTransshrp }
