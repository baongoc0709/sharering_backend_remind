const mongoose = require('mongoose')
const Schema = mongoose.Schema


const keaztransaction = new Schema({
  transaction_id :  { type: String, required: true  },
  from_address: { type: String, required: true  },
  to_address  : { type: String,required: true },
  status   : { type: String,required: true ,default : 0 },
  net     : { type: Number },
  amount : { type :Number },
  fee : { type : Number },
  _created: { type: Date, required: true, default: Date.now },
  _updated: { type: Date, required: true, default: Date.now }
})
keaztransaction.pre('update', function() {
  this.update({},{ $set: { _updated: new Date() } })
})


const keazprivate = new Schema({

  address: { type: String, required: true  },
  publickey: { type: String },
  privatekey : { type :String , required :true },
  _created: { type: Date, required: true, default: Date.now },
  _updated: { type: Date, required: true, default: Date.now }
})
keazprivate.pre('update', function() {
  this.update({},{ $set: { _updated: new Date() } })
})

const privatekeaz = mongoose.model('keazprivate', keazprivate)
const keaz   = mongoose.model('keaztransaction', keaztransaction)

module.exports = { keaz ,privatekeaz }
