const mongoose = require('mongoose')
const Schema = mongoose.Schema

const logFCM = new Schema({
  address : { type: String }, //transaction top up , backend create
  type     : { type: Number,required: true ,default : 0 }, //0   ,
  status   : { type: Number,required: true ,default : 0 }, //0 : unread , 1 read
  data     : { type :Object },
  _created: { type: Date, required: true, default: Date.now },
  _updated: { type: Date, required: true, default: Date.now }
})
logFCM.pre('update', function() {
  this.update({},{ $set: { _updated: new Date() } })
})
const logFCMfunction   = mongoose.model('logFCM',logFCM)

module.exports = { logFCMfunction }

const fcmtoken = new Schema({

  address: { type: String, required: true },
  token: { type: String,required: true },
  deviceid: { type: String ,required: true ,unique : true },
  _created: { type: Date, required: true, default: Date.now },
  _updated: { type: Date, required: true, default: Date.now }
  //platform: { type: String ,required: true },
  //accessToken: { type: String, required: true },
  //name: { type: String, required: true },
})

fcmtoken.pre('update', function() {
  this.update({},{ $set: { _updated: new Date() } })
})
const logFCMmodel   = mongoose.model('logFCM', logFCM)
const fcmtokenModel   = mongoose.model('fcmtoken', fcmtoken)
module.exports = { logFCMmodel ,fcmtokenModel }
