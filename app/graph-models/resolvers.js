
const casual = require('casual')


//const authors = [
//  { id: 10, username: 'johndoe', first_name: 'John', last_name: 'Doe', avatar_url: 'acme.com/avatars/10' },
// { id: 11, username: 'janedoe', first_name: 'Jane', last_name: 'Doe', avatar_url: 'acme.com/avatars/11' }
//]
//const stats = [
//  { tweet_id: 1, views: 123, likes: 4, retweets: 1, responses: 0 },
//  { tweet_id: 2, views: 567, likes: 45, retweets: 63, responses: 6 }
//]

const first_resolvers = {
  Query: {
    getBalance() {
      return { address: 'ssss', height: casual.integer(1000, 10000) }
    }

  },
  Balance: {
    coins() {
      return [
        { denom: 'SHR', amount: casual.integer(0, 1000) }
      ]
    }
  }

}

module.exports = first_resolvers
