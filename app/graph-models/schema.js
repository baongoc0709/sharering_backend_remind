

//type Query {
//  getBalance(address: String): Balance
//}
const graphTools = require('graphql-tools')
const first_resolvers = require('./resolvers')
const schema_register = `

type Balance {
  address: String
  height: Int
  coins: [Coin]
}

type Coin {
  denom: String
  amount: Int
}

scalar Url
scalar Date

type Query {
    getBalance(address: String): Balance
  }

`
const schemas = [schema_register]
const resolvers = [first_resolvers]
const option  = {
  typeDefs : schemas,
  resolvers
}

const schema = graphTools.makeExecutableSchema(option)

module.exports = schema
