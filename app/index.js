//import ShareLedger from './libs/blockchain'

//import ShareLedger from "./libs/blockchain";

const fs = require('fs')
const path = require('path')
console.log(path)
const restify = require('restify')
import { graphqlRestify, graphiqlRestify  } from 'apollo-server-restify'
const status = require('./middlewares/errorhandle')
// log to multi stream
const multistream = require('pino-multi-stream').multistream

// add flag a to append log
// `{ level: 'info', stream: fs.createWriteStream(`${__dirname}/logs/info.out`, { 'flags': 'a' }) }`

const streams = [
  { level: 'debug', stream: process.stdout },
  { level: 'info', stream: fs.createWriteStream(`${__dirname}/logs/info.out`) }
]
// logging object
const pino = require('pino')({
  name: 'ShareRing Platform',
  level: 'debug'
}, multistream(streams))

// add cors header to fix methods don't allow with graphql
const corsMiddleware = require('restify-cors-middleware')
const cors = corsMiddleware({
  preflightMaxAge: 5,
  origins: ['*'],
  allowHeaders: ['Origin', 'Accept', 'Accept-Version', 'Content-Length', 'Content-MD5', 'Content-Type', 'Date', 'X-Api-Version', 'X-Response-Time', 'X-PINGOTHER', 'X-CSRF-Token,Authorization'],
  //allowMethods: ['*']
  allowMethods 	:['POST', 'GET', 'OPTIONS']
})

const configs = require('./configs')
const mongoose = require('mongoose')
mongoose.Promise = global.Promise

// const MONGO_URL =  `mongodb://${configs.MONGO_USER}:${configs.MONGO_PASS}@${configs.MONGO_HOST}:${configs.MONGO_PORT}/${configs.MONGO_DB}`)
//mongoose.connect('mongodb://127.0.0.1:27017',{ useMongoClient: true })
//const db = mongoose.connection

mongoose.connect('mongodb://118.69.238.23:27017/admin', {
  user: 'root',
  pass: '123!@#',
  useNewUrlParser: true
}, (err) => {
  if (err) {
    console.log('An error occurred while attempting to connect to MongoDB', err)
    process.exit(1)
  }

  console.log('%s v%s ready to accept connections on port %s in %s environment.')

})
const schema = require('./graph-models/schema')
const server = restify.createServer({
  title: 'Apollo Server',
  log: pino
})
require('./graph-models/keaz/keaz.model')
const callGenAccountAPIFunction = require('./controllers/keaz.controller').callGenAccountAPI
const createSignTxFunction = require('./controllers/keaz.controller').createSignTx
const getHashTxShareLedgerFunction = require('./controllers/keaz.controller').getHashTxShareLedger
const makeTransactionFunction = require('./controllers/keaz.controller').makeTransaction
const createPushSHRPsigntxFunction = require('./controllers/keaz.controller').createPushSHRPsigntx
const keaztransactionmodel = mongoose.model('keaztransaction')
const keazprivatemodel = mongoose.model('keazprivate')

server.post('/image',  (req, res) => {
  try {
    const base64Img = require('base64-img')
    const ret = {
      status: status[200],
      success: true,
      message: '',
      data: []
    }
    const filepath1 = `./images/${req.body.userid}/${req.body.image_type}/`
    const retpath = `http://192.168.1.234/images/${req.body.userid}/${req.body.image_type}/`
    //const filename = new Date().getTime()
    console.log(req.body.data.length)
    for (let k = 0 ; k < req.body.data.length ; k++) {
      const filename = new Date().getTime() + k
      const v = req.body.data[k]
      base64Img.img(v, filepath1, filename, (err, filepath) => {
        if (err) {
          ret.message = err
          res.send(ret)
        } else {
          console.log(filepath)
          ret.data.push(retpath)
          ret.message = 'success'
          if (k === req.body.data.length -1) {
            console.log(ret)
            res.send(ret)
          }
        }
      })
    }
  }catch (e) {
    console.log('get block error ',e)
  }
})

server.post('/v1/wallet/bulk',  (req, res) => {
  const ret = {
    status: status[200],
    success: true,
    message: '',
    data: []
  }
  try {
    for (let k = 0 ; k < req.body.length ; k++) {
      const retKeaz = {
        user_id : '',
        user_type : '',
        wallet_address : ''
      }
      callGenAccountAPIFunction().then(shrobj => {
        const keazobj = new keazprivatemodel()
        keazobj.address =  shrobj.address
        keazobj.privatekey = shrobj.privatekey
        keazobj.save()
        retKeaz.user_id  = req.body[k].user_id
        retKeaz.user_type = req.body[k].user_type
        retKeaz.wallet_address = shrobj.address
        ret.data.push(retKeaz)
        if (k === req.body.length - 1)
          res.send(ret)
        
      })
    }

  }catch (e) {
    console.log('get block error ',e)
    ret.message = e
    res.send(ret)
  }
})

server.post('/v1/wallet',  (req, res) => {
  const ret = {
    status: status[200],
    success: true,
    message: '',
    data: {}
  }
  try {

    const objKeaz = {
      user_id : '',
      user_type : '',
      wallet_address : ''
    }
    callGenAccountAPIFunction().then(shrobj => {
      const keazobj = new keazprivatemodel()
      keazobj.address =  shrobj.address
      keazobj.privatekey = shrobj.privatekey
      keazobj.save()
      objKeaz.user_id  = req.body.user_id
      objKeaz.user_type = req.body.user_type
      objKeaz.wallet_address = shrobj.address
      ret.data = objKeaz
      
    })


  }catch (e) {
    console.log('get block error ',e)
    ret.message = e
    res.send(ret)
  }
})

server.post('/v1/payment',(req, res) => {
  const ret = {
    status: status[200],
    success: true,
    message: '',
    data: {
      transaction_id : '',
      status : 'processed',
      to_address : req.body.to_address,
      from_address :req.body.from_address,
      net : req.body.amount - 0.02,
      amount : req.body.amount,
      fee : 0.02
    }
  }
  try {
    const response =  new Promise((resolve) => {
      const query = { 'address': req.body.from_address }
      keazprivatemodel.findOne(query).then(keazObj => {
        console.log('my private key')
        console.log(keazObj.privatekey)
        console.log(keazObj.address)
        console.log(req.body.from_address)
        createSignTxFunction(keazObj.privatekey,keazObj.address ,req.body.to_address, req.body.amount).then(signTx => {
          getHashTxShareLedgerFunction(signTx).then(hashtx => {
            const keaztransactionobj = new keaztransactionmodel()
            keaztransactionobj.fee = 0.02
            keaztransactionobj.net = req.body.amount - 0.02
            keaztransactionobj.amount = req.body.amount
            keaztransactionobj.status = 'processed'
            keaztransactionobj.from_address = req.body.from_address
            keaztransactionobj.to_address = req.body.to_address
            keaztransactionobj.transaction_id = hashtx
            keaztransactionobj.save()
            ret.data.transaction_id = hashtx
            createPushSHRPsigntxFunction(req.body.from_address, req.body.amount ).then(makeMoneysignTx => {
              makeTransactionFunction(makeMoneysignTx).then(makemoneyData => {
                console.log('make money data')
                console.log(makemoneyData)
                makeTransactionFunction(signTx).then(data => {
                  console.log(data)
                })
              })
            })
            console.log(ret)
            resolve(ret)
          })
        })
      })
    })
    response.then(data =>{
      res.send(data)
    })

  }catch (e) {
    console.log('get block error ',e)
    ret.message = e
    res.send(ret)
  }
})

//const CronJobFunctionTest = require('./controllers/cron.controller').CronJobFunction
//const datainput = {}
//CronJobFunctionTest('* * * * * *',datainput)

const graphQLOptions = { schema: schema }
server.pre(cors.preflight)
server.use(cors.actual)
server.use(restify.plugins.bodyParser())
server.use(restify.plugins.queryParser())

server.post('/graphql', graphqlRestify(graphQLOptions))
server.get('/graphql', graphqlRestify(graphQLOptions))
server.get('/graphiql', graphiqlRestify({ endpointURL: '/graphql' }))

// config router
//require('./routers')(server)

server.listen(configs.NODE_PORT, () => pino.info(`Listening on ${configs.NODE_PORT}`))
