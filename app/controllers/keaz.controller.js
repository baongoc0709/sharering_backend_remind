
import ShareLedger from 'shareledger_client'
import SHRCrypto from 'shr-keys'
const TCPClientSHL = 'http://118.69.238.23:56657'

function callGenAccountAPI() {
  const ret = {
    address : '',
    privatekey : ''
  }
  try {
    return new Promise((resolve) => {
      const mne = SHRCrypto.KeyPair.createMnemonic()
      if (mne) {
        const kp = SHRCrypto.KeyPair.fromMnemonic(mne)
        console.log(kp)
        ret.address = kp.address.toUpperCase()
        ret.privatekey = kp.privKey
        resolve(ret)
      }
    })
  } catch (e) {
    console.log(e)
    return ret
  }
}

function createSignTx (privateK,from_address,to_address,shrp) {

  const privateKey = privateK

  const acc = SHRCrypto.KeyPair.fromPrivateKey(privateKey)

  const nonceQueryTx = ShareLedger.createNonceQueryTx(from_address)

  const Client = ShareLedger.Client
  const client = new Client(TCPClientSHL)

  return new Promise((resolve, reject) => {
    client.query(nonceQueryTx).then((result) => {

      console.log('Balance:', result.response.log)
      const nonce = parseInt(result.response.log)
      const creditTx = ShareLedger.createSendTx(to_address.toUpperCase(), shrp, 'SHRP', nonce + 1, acc)
      console.log('my sign tx')
      console.log(creditTx)
      resolve(creditTx)
    })
      .catch(error => {
        reject(error)
      })
  })
}

function createPushSHRPsigntx (wallet_address,shrp) {

  const privateKey = 'ab83994cf95abe45b9d8610524b3f8f8fd023d69f79449011cb5320d2ca180c5'

  const acc = SHRCrypto.KeyPair.fromPrivateKey(privateKey)

  const nonceQueryTx = ShareLedger.createNonceQueryTx('405C725BC461DCA455B8AA84769E8ACE6B3763F4')

  const Client = ShareLedger.Client
  const client = new Client(TCPClientSHL)

  return new Promise((resolve, reject) => {
    client.query(nonceQueryTx).then((result) => {
      console.log('Balance:', result.response.log)
      const nonce = parseInt(result.response.log)
      const creditTx = ShareLedger.createCreditTx(wallet_address.toUpperCase(), shrp, 'SHRP', nonce + 1, acc)
      console.log('Tx:', creditTx)
      resolve(creditTx)
    })
      .catch(error => {
        reject(error)
      })
  })
}

function getHashTxShareLedger(signtx) {

  return new Promise((resolve,reject) => {
    try {
      const hashTx = ShareLedger.getTxHash(signtx)
      console.log(hashTx)
      resolve(hashTx)
    } catch (e) {
      reject(e)
    }
  })
}


function makeTransaction(signTx)  {
  try {

    const Client = ShareLedger.Client
    const client = new Client(TCPClientSHL)

    return new Promise((resolve) => {
      client.sendTx(signTx).then(data => {
        resolve(data)
      })
    })
  } catch (e) {
    console.log('send tx error ',e)
  }

}
/*constructor(privKey, pubKey, address) {
  this.privKey = privKey
  this.pubKey = pubKey
  this.address = address.toUpperCase()
}*/


module.exports = { callGenAccountAPI ,createSignTx ,getHashTxShareLedger,makeTransaction ,createPushSHRPsigntx }
