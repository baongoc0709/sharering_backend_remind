const mongoose = require('mongoose')
require('./../graph-models/transaction/transaction.model')
const CronJob = require('cron').CronJob
const logTransshrp = mongoose.model('logTransactionSHRP')
const senddingFcm  =  require('./fcm.controller')

function CronJobFunction(settingcron,datainput) {

  try {
    const query = { 'receive_address': datainput.receive_address }
    logTransshrp.find(query).then((data) => {
      console.log(data)
      new CronJob(settingcron, () => {
        console.log(data)
        const fcmdata = {
          'des_address': data.des_address,
          'source_address': data.source_address,
          'message': datainput.message,
          'type': 'reminder'
        }
        senddingFcm.sendKycNotification(fcmdata)
      }, null, true, 'America/Los_Angeles')
    })
  } catch (e) {
    console.log(e)
  }

}


module.exports = { CronJobFunction }
