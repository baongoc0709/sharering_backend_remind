const FCM = require('fcm-push')
const serverkey = 'AAAAZLew6hA:APA91bGfwH8yqwYZuxrkBm6IeABqktkFcfV9gu8ONu4soWTQrZRQQJgk7Nk_B8NS6YVUpZgdJYjBKbs_UBcSQkTQLSa_Ol57zhhVEE--c41rczeQ-CEdERd7lAHRlNt4gJQZKck5x-at'
const fcm = new FCM(serverkey)
//require('./../graph-models/user/user.model')
require('./../graph-models/fcm/fcm.model')
const mongoose = require('mongoose')

const logFCMmodel = mongoose.model('logFCM')
const Fcmtoken = mongoose.model('fcmtoken')

function sendKycNotification (req,res )  {
  try {

    const query = { 'address' : req.des_address }
    Fcmtoken.findOne(query).exec((errfind, datafcn) => {
      if (errfind)
        console.log(errfind)
      if (datafcn) {
        const ret  = datafcn.token
        const datafcm = {
          priority:'high',
          body:req.message,
          title: 'ShareRing',
          type : req.type,
          source_address: req.source_address,
          publickey:req.publickey ,
          des_address:req.des_address
        }
        const message = {
          //to : 'fBrOZ6_NbjA:APA91bGNzT1KCuz8lnk1dWhXg5wVv9Nw28xpzNdW7fxNTIj88AROPlLoHybZ-QuYCid-AIVVJ8RLlxV7-t41thaX3QHU21ltq5_Hezaey2loH3R12HZr71CdKVoW2eH6qB00sBbNrMMRJvvywQBlQ7TGZrHe9v7QWg',
          to : ret,
          data: {
            custom_notification: datafcm
          },
          notification: datafcm
        }
        const myLogFCMObj =  logFCMmodel()
        myLogFCMObj.address = req.des_address
        myLogFCMObj.data = datafcm
        myLogFCMObj.type = req.type
        myLogFCMObj.status = 0
        myLogFCMObj.save()
        fcm.send(message, (err,response) => {
          if(err) {
            console.log(`Something has gone wrong !${err}`)
            return err}
          console.log('Successfully sent with resposne :',response)
          res = response
          return res
        })
      }
    })
  } catch (e) {
    console.log('get fcm token error ',e)
  }

}

module.exports = { sendKycNotification }
