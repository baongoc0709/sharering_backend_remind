const packages = require( `${process.cwd()}/package.json` )

process.env.NODE_ENV = process.env.NODE_ENV || 'development'
const configs = process.env
configs.NAME = packages.name
configs.VERSION = packages.name

module.exports = configs
