import { MongoClient } from 'mongodb'

const configs = require('./configs')


const start = async () => {
  // make database connections
  //const pgClient = new Client('postgresql://localhost:3211/foo');
  //await pgClient.connect();
  const MONGO_URL =  `mongodb://${configs.MONGO_HOST}:${configs.MONGO_PORT}`
  const db =  await MongoClient.connect(MONGO_URL,{ useNewUrlParser: true })

  const Posts = db.collection('posts')
  console.log(Posts)
}

module.exports = start
